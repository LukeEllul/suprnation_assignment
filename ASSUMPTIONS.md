### Only intergers are processed

The program can only validate and evaluate expressions that include integers. Decimal values include the "." character and I did not have enough time to work around this type of validation.

### Trigonomtery functions are not evaluated

Trignometry functions are validated, so for example if you input: "cos(30" the validation picks the mistake. However, due to time constraints, I did not have enough time to implement the evalaution of trigonometry functions. A little string manipulation is needed, but I don't have 30 mins.

### RAND Operand

I didn't understand the exact question, however I implemented a function that fetches the random number from the designated URL.

### Language Used

All the functions where implemented in JavaScript, however, Haskell notation was used to document the function type parameters. This is purely for documentation and readability issues, no proper type system was implemenetd. 