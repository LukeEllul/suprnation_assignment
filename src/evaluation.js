import * as R from 'ramda';
import {checkExpression} from './validation';

/**
 * compute :: String -> Number -> Number -> Number
 */
const compute = R.curry((operator, n1, n2) => {
  switch(operator){
    case '+': return n1 + n2;
    case '-': return n1 - n2;
    case '/': return n1 / n2;
    case '*': return n1 * n2;
  }
});

/**
 * evaluate :: String -> Number
 */
export const evaluate = s => checkExpression(([n, exp], [n1, e]) => {
  if(exp === '' && n1 === 0) return [0, [n1, e]];
  if(exp === '') return e;
  if(n1 === 0) return [exp, [n1, e]];
  if(Array.isArray(exp)){
    const [r, [_, operator]] = exp;
    return compute(operator, Number(r), Number(e));
  }
  return exp;
})([-2, s, '']);