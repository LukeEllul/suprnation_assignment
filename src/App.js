import React from 'react';

import SuprCalculator from './SuprCalculator';
import Calculator from './Calculator';

function App() {
  return (
    <div className="App">
      <Calculator/>
      <br/>
      <SuprCalculator/>
    </div>
  );
}

export default App;
