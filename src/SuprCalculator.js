import React, { useState, useEffect } from 'react';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles';

import { isExpression } from './validation';
import { evaluate } from './evaluation';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
});

const SuprCalculator = ({ classes }) => {
  const [expression, setExpression] = useState('');
  const [result, setResult] = useState(0);
  const [valid, setValid] = useState(false);

  useEffect(() => {
    const i = isExpression(expression);
    setValid(i);
    if(i) setResult(evaluate(expression));
  }, [expression]);

  function FormRow({ numbers }) {
    return (
      <React.Fragment>
        {numbers.map(n => (
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Button onClick={() => setExpression(expression + n)}>{n}</Button>
            </Paper>
          </Grid>
        ))}
      </React.Fragment>
    );
  }

  function Operators() {
    return (
      <React.Fragment>
        {['+', '-', '*', '/'].map(op => (
          <Grid item xs={3}>
            <Paper className={classes.paper}>
              <Button onClick={() => setExpression(expression + op)}>{op}</Button>
            </Paper>
          </Grid>
        ))}
      </React.Fragment>
    );
  }

  return (
    <div className={classes.root}>
      <h1>Supr-Calculator</h1>
      <br/>
      <Grid container spacing={1}>
        <Grid item xs={12} spacing={3}>
          <Paper className={classes.paper}>{`${expression} = ${valid ? result : 0}`}</Paper>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <FormRow numbers={[1, 2, 3]} />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <FormRow numbers={[4, 5, 6]} />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <FormRow numbers={[7, 8, 9]} />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Button onClick={() => setExpression(expression + '0')}>0</Button>
            </Paper>
          </Grid>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <Operators />
        </Grid>
      </Grid>
      <Button onClick={() => setExpression('')}>Clear</Button>
    </div>
  );
}

export default withStyles(styles)(SuprCalculator);