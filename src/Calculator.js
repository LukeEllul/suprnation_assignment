import React, { useState, useEffect } from 'react';
import { isExpression } from './validation';
import { evaluate } from './evaluation';

import * as R from 'ramda';

export default ({ }) => {
  const [expression, setExpression] = useState('');
  const [valid, setValid] = useState(false);
  const [expressions, setExpressions] = useState([]);
  const [result, setResult] = useState('');

  useEffect(() => {
    setValid(isExpression(expression));
  }, [expression]);

  return (
    <div>
      <h1>Calculator</h1>
      <div style={{
        display: 'flex'
      }}>
        <input type="text" value={expression} onChange={e => setExpression(e.target.value)} />
        <button disabled={!valid} onClick={() => {
          const thisResult = evaluate(expression);
          setResult(thisResult);
          setExpressions([...expressions.length > 4 ? expressions.slice(-4) : expressions, { expression, result: thisResult }]);
        }}>
          =
      </button>
        <h3>{result}</h3>
      </div>
      <div>
        <h4>Prev. Expressions</h4>
        <div>
          {expressions.map(({ expression, result }) => (
            <h4>{`${expression} = ${result}`}</h4>
          ))}
        </div>
      </div>
      <label hidden={valid || R.isEmpty(expression)}>Invalid Expression</label>
    </div>
  )
}