import * as R from 'ramda';

/**
 * isOperator :: String -> [Int, String, String]
 */
const isOperator = op => R.any(R.equals(R.head(op)))(['+', '-', '*', '/']) ? [0, R.tail(op), R.head(op)] : [-1, op, op];

/**
 * isDigit :: String -> [Int, String, String]
 */
const isDigit = op => !isNaN(R.head(op)) ? R.compose(digits => [1, R.drop(digits.length, op), digits], R.takeWhile(R.compose(R.not, isNaN)))(op) : [-1, op, op];

/**
 * isTrig :: String -> [Int, String, String]
 */
const isTrig = op => {
  const s = R.any(R.equals(R.take(4, op)))(['sin', 'cos', 'tan'].map(s => s + '(')) &&
    R.compose(([_, s]) => R.head(s) === ')' && R.tail(s), isDigit, R.drop(4))(op);

  return s || s === '' ? [2, s, R.take(op.length - s.length, op)] : [-1, op, op];
}

/**
 * or :: [(String -> [Int, String])] -> String -> [Int, String, String]
 */
const or = R.curry((fs, s) => R.isEmpty(fs) ? [-1, s, s] : 
  R.compose(([n, a, e]) => n === -1 ? or(R.tail(fs), s) : [n, a, e])(R.head(fs)(s)));

/**
 * getFunctions :: Int -> [(String -> [Int, String])]
 */
const getFunctions = n => {
  switch(n){
    case 0: return [isDigit, isTrig];
    case 1: return [isOperator];
    case 2: return [isOperator];
  }
};

/**
 * checkExpression :: (([Int, String], [Int, String]) -> a) -> [Int, String, String] -> (False | a)
 */
export const checkExpression = f => ([n, s, e]) => (s === '' || n === -1) ? (n !== 0 && n !== -1 && e) : 
  R.compose(([n1, s1, e1]) => checkExpression(f)([n1, s1, f([n, e], [n1, e1])]), or(n === -2 ? [isOperator, isDigit, isTrig] : getFunctions(n)))(s);

/**
 * isExpression :: String -> Boolean
 */
export const isExpression = s => checkExpression(R.identity)([-2, s, '']) && true;